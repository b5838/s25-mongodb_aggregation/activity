// Count total number of fruits on sale

db.fruits.aggregate([
    {"$match":{"onSale": true}},
    {$group:{
        "_id": "$onSale",
        "fruitsOnSale":{
            "$count":{}
        }
    }},
    {$project:{"_id": 0}}
]
);

// Count total number of fruits with stock more than 20.
db.fruits.aggregate([
    {"$match":{"stocks": {$gte: 20}}},
    {$group:{
        "_id": "$stocks",
        "enoughStock":{
            "$count":{}
        }
    }},
    {$project:{"_id": 0}}
]
);

// Average price of fruitsOnSale per supplier
db.fruits.aggregate([
    {"$match":{"onSale": true}},
    {$group:{
        "_id": "$supplier_id",
        "avg_price":{$avg:"$price"},
    }},
    {$sort: {"total": -1}}
]
);

// Highest price of a fruit per supplier.
db.fruits.aggregate([
    {$group:{
        "_id": "$supplier_id",
        "max_price":{$max:"$price"}}
    },
]
);

// Lowest price of a fruit per supplier
db.fruits.aggregate([
    {$group:{
        "_id": "$supplier_id",
        "min_price":{$min:"$price"}}
    },
]
);
